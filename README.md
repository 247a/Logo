# Logo Usage instuctions
This logo is designed in inkscape
This logo is a simple design consisting of a circle, a square and a triangle.
It also contains three coulors in an order that if printed in black and white or in greyscale the logo is still distinctive.

## variants
There are four variants which can be split into first two categorys: 
 Black and white,
 full colour.
After that split the logo can be split further to the four stages of:
 no text,
 247a floating above,
 247a floating below,
 247a burnt inside the triangle.
## future work
Animated intros


## Usage
for origanl svg use drawing_2.svg as it is moved and resized

for android add logo_black_white.xml and logo_color.svg to drawable folder (Android Studio 1.4 or higher)

for other uses use the drawing.png

